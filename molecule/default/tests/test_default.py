import json
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    with host.sudo():
        cmd = host.run('docker inspect docker')
        inspection = json.loads(cmd.stdout)
        assert inspection[0][u'Name'] == '/docker'
        assert inspection[0][u'NetworkSettings'][u'Networks'][u'docker-network']
